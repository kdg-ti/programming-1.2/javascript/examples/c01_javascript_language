let cookiesBaked = 0;
function bakeCookie(cookieType){
    console.log("Baking a delicious " + cookieType + " cookie. Please wait!");
    setTimeout( () => {
        cookiesBaked++;
        console.log(`All done! Enjoy your ${cookieType} cookie!`)
    },1000);

}

export {cookiesBaked, bakeCookie};